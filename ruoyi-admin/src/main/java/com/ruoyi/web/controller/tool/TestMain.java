package com.ruoyi.web.controller.tool;

public class TestMain {

    public static int getLevenshteinDistance(String X, String Y)
    {
        int m = X.length();
        int n = Y.length();

        int[][] T = new int[m + 1][n + 1];
        for (int i = 1; i <= m; i++) {
            T[i][0] = i;
        }
        for (int j = 1; j <= n; j++) {
            T[0][j] = j;
        }

        int cost;
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                cost = X.charAt(i - 1) == Y.charAt(j - 1) ? 0: 1;
                T[i][j] = Integer.min(Integer.min(T[i - 1][j] + 1, T[i][j - 1] + 1),
                        T[i - 1][j - 1] + cost);
            }
        }

        return T[m][n];
    }

    public static double findSimilarity(String x, String y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException("Strings must not be null");
        }

        double maxLength = Double.max(x.length(), y.length());
        if (maxLength > 0) {
            // 如果需要，可以选择忽略大小写
            return (maxLength - getLevenshteinDistance(x, y)) / maxLength;
        }
        return 1.0;
    }

    public static void main(String[] args) {
        String s1 = "这 Leven3htein 距离(或编辑距离) 算法3通过计算将一个3字符串转换为12另一个字符串所需的最小操作数来判断三个字符串之间的差异程度。";
        String s2 = "这 Levenshtein 距离(或编辑距离) 算法通过计算将一个字34符串转换为另一5个字符串所需的最1小操作数来判断两个5字符串之间的差异程度。";
        long l = System.currentTimeMillis();
        double similarity = findSimilarity(s1, s2);
        long l1 = System.currentTimeMillis();
        System.out.println(l1 - l);
        System.out.println(similarity);
    }
}
