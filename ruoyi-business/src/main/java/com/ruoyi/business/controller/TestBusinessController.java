package com.ruoyi.business.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.business.domain.MySysNotice;
import com.ruoyi.business.service.IMySysNoticeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.page.TableSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.io.InputStreamReader;

@RestController
@RequestMapping("/test")
public class TestBusinessController extends BaseController {

    @Autowired
    IMySysNoticeService mySysNoticeService;

    @GetMapping("/get")
    public TableDataInfo getss() {
        PageDomain pageDomain = TableSupport.getPageDomain();
        Page<MySysNotice> page = new Page<MySysNotice>(pageDomain.getPageNum(), pageDomain.getPageSize());
        IPage<MySysNotice> page1 = mySysNoticeService.page(page);
        return getDataTable(page1);
    }

    public static void main(String[] args) {
        try {

            ProcessBuilder processBuilder = new ProcessBuilder();
            //设置第三方应用程序的命令
            //processBuilder.command("ping","127.0.0.1");
            processBuilder.command("ipconfig","-all");

            //将标准输入流和错误流合并
            processBuilder.redirectErrorStream(true);
            //启动一个进程（相当于在命令行中输入命令）
            Process process = processBuilder.start();

            //通过标准输入流来拿到正常和错误的信息
            InputStream inputStream = process.getInputStream();

            //转成字符流
            InputStreamReader reader = new InputStreamReader(inputStream, "gbk");
            //缓冲
            char[] chars = new char[1024];
            int len = -1;
            while ((len = reader.read(chars)) != -1) {
                String string = new String(chars, 0, len);
                System.out.println(string);
            }
            inputStream.close();
            reader.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
