package com.ruoyi.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.business.domain.MySysNotice;

public interface MySysNoticeMapper extends BaseMapper<MySysNotice> {
}
