package com.ruoyi.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.business.domain.MySysNotice;

public interface IMySysNoticeService extends IService<MySysNotice> {
}
