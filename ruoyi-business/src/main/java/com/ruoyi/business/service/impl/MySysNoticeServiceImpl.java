package com.ruoyi.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.domain.MySysNotice;
import com.ruoyi.business.mapper.MySysNoticeMapper;
import com.ruoyi.business.service.IMySysNoticeService;
import org.springframework.stereotype.Service;

@Service
public class MySysNoticeServiceImpl extends ServiceImpl<MySysNoticeMapper, MySysNotice> implements IMySysNoticeService {
}
