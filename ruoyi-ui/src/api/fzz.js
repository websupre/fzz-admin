import request from '@/utils/request'

export function userAdd(data) {
    return request({
        url: '/user/add',
        method: 'post',
        data
    })
}

export function userList(data) {
    return request({
        url: '/user/page',
        method: 'post',
        data
    })
}

export function userUpdate(data) {
    return request({
        url: '/user/update',
        method: 'post',
        data
    })
}

export function userDel(data) {
    return request({
        url: '/user/del',
        method: 'post',
        data
    })
}

export function getInfo(id) {
    return request({
        url: `/user/info/${id}`,
        method: 'get',
    })
}

export function uploadImg(data) {
    return request({
        url: '/common/upload',
        method: 'post',
        data: data
    })
}

export function chatList(data) {
    return request({
        url: '/chat/page',
        method: 'post',
        data
    })
}
export function chatDel(data) {
    return request({
        url: '/chat/del',
        method: 'post',
        data
    })
}
export function getChatInfo(id) {
    return request({
        url: `/chat/info/${id}`,
        method: 'get',
    })
}
export function geSeeks(id) {
    return request({
        url: `/chat/getSeeks/${id}`,
        method: 'get',
    })
}

export function getAllConfig() {
    return request({
        url: `/config/getAllConfig`,
        method: 'get',
    })
}

export function upAllConfig(data) {
    return request({
        url: `/config/upAllConfig`,
        method: 'post',
        data
    })
}

export function docPage(data) {
    return request({
        url: `/chat/docPage`,
        method: 'post',
        data
    })
}

export function contractPage(data) {
    return request({
        url: `/chat/contractPage`,
        method: 'post',
        data
    })
}



export function sumData(data) {
    return request({
        url: `/count/sumData`,
        method: 'post',
        data
    })
}
export function userSumData(data) {
    return request({
        url: `/count/userSumData`,
        method: 'post',
        data
    })
}
export function chatSumData(data) {
    return request({
        url: `/count/chatSumData`,
        method: 'post',
        data
    })
}

export function downloadDoc(urls) {
    return request({
        url: `/aiapi/download_doc?filename=${urls}`,
        method: 'get',
    })
}

export function downloadContract(url) {
    return request({
        url: `/aiapi/download_contract?filename=${url}`,
        method: 'get',
    })
}